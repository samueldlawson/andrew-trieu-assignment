package com.halifax.branch.provider;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.halifax.branch.domain.Branch;


@RunWith(SpringRunner.class)
@SpringBootTest
public class HalifaxProviderTest {
	
	@Autowired
	HalifaxProvider halifaxProvider;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetAllBranches() {
		List<Branch> branches = halifaxProvider.getAllBranches();
		assertEquals(597, branches.size());		
	}

	@Test
	public void testGetBranches() {
		List<Branch> branches = halifaxProvider.getBranches("Liverpool");
		assertEquals(7, branches.size());
		
		branches = halifaxProvider.getBranches("xyz");
		assertEquals(0, branches.size());
		assertNotNull(branches);		
	}

}
