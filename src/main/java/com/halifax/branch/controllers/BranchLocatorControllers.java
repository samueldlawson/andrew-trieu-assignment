package com.halifax.branch.controllers;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.halifax.branch.domain.Branch;
import com.halifax.branch.provider.HalifaxProvider;


/**
 * This is the Rest controller class for the Branch locator for the Halifax project
 * with URI as /halifax.
 * @author atrieu
 *
 */
@RestController
@RequestMapping("/halifax")
public class BranchLocatorControllers {
	
	private HalifaxProvider halifaxProvider;
	final private HttpHeaders httpHeaders;

	public BranchLocatorControllers(HalifaxProvider halifaxProvider) {
		this.halifaxProvider = halifaxProvider;
		httpHeaders= new HttpHeaders();
	}
	
	@GetMapping("/branches")	
	public ResponseEntity<List<Branch>> getBranches() {
		List<Branch> branches = halifaxProvider.getAllBranches();
	    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		if (branches.size() != 0) {
			return new ResponseEntity<List<Branch>>(branches, httpHeaders, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<Branch>>(branches, httpHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
		}		
	}
	
	@GetMapping("/branches/{city}")
	public ResponseEntity<List<Branch>> getBranches(@PathVariable String city) {
		List<Branch> branches = halifaxProvider.getBranches(city);
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		if (branches.size() != 0) {
			return new ResponseEntity<List<Branch>>(branches, httpHeaders, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<Branch>>(branches, httpHeaders, HttpStatus.NOT_FOUND);
		}
	}
}
