package com.halifax.branch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HalifaxBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(HalifaxBankApplication.class, args);
	}

}
