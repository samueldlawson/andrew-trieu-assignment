package com.halifax.branch.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class stores Branch information as per the requirement
 * 
 * @author trieu
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Branch {
	
	private String branchName;
	private float latitude;
	private float longitude;
	private String streetAddress;
	private String city;
	private String countrySubDivision;
	private String country;
	private String postCode;

}
