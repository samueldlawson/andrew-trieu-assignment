package com.halifax.branch.provider;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.halifax.branch.domain.Branch;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import net.minidev.json.JSONArray;

/**
 * This is a Provider class.  It is calling the REST service from the Halifax website
 * to get the Bank Branch information.  Since the requirement is just a subset of the
 * Halifax Branch, this class will only transform the necessary data into the Branch object.
 * 
 * @author atrieu
 *
 */
@Component
public class HalifaxProvider {

	private RestTemplate restTemplate;
	private String host;
	private String branchesUri;

	/**
	 * Constructor for the Halifax provider.  It is getting the host and branches uri from the resource file.
	 * @param host			The host address to the Halifax
	 * @param branchesUri	The URI to the branches webservice.
	 */
	public HalifaxProvider(@Value("${halifax.host}") String host, @Value("${halifax.uri}") String branchesUri) {
		restTemplate = new RestTemplate();
		this.host = host;
		this.branchesUri = branchesUri;
	}

	
	/**
	 * This method will call the halifax url to get the JSON objects for the branch and it transform
	 * the data into the list of Branch as per the requirements.
	 * 
	 * @return ArrayList<Branch>
	 */
	public List<Branch> getAllBranches() {
		String url = host + branchesUri;
		String jsonpathBranch = "$..Branch";
		DocumentContext jsonContext = JsonPath.parse(restTemplate.getForEntity(url, String.class).getBody());
		JSONArray brandName = jsonContext.read(jsonpathBranch);
		
		List<Branch> branches = new ArrayList<Branch>();
		
		for(int i = 0; i < brandName.size(); i++) {
			JSONArray jsonArray = (JSONArray)brandName.get(0);
			
			for (int j = 0; j < jsonArray.size(); j++) {
				LinkedHashMap<String, Object> item = (LinkedHashMap<String, Object>)jsonArray.get(j);
				String branchName = (String)item.get("Name");
				
				LinkedHashMap<String, Object> address = (LinkedHashMap<String, Object>)item.get("PostalAddress");
				JSONArray addressLine = (JSONArray)address.get("AddressLine");
				
				String streetAddress = (String) addressLine.get(0);
				String city = (String)address.get("TownName");
				String postcode = (String)address.get("PostCode");
				JSONArray countrySubDiv = (JSONArray)address.get("CountrySubDivision");
				String countrySubDivision = null;
				if (countrySubDiv != null) {
					countrySubDivision = (String)countrySubDiv.get(0);					
				}
				
				String country = (String)address.get("Country");
				LinkedHashMap<String, Object> geoLocation = (LinkedHashMap<String, Object>)address.get("GeoLocation");
				LinkedHashMap<String, Object> geographicCoordinates = (LinkedHashMap<String, Object>)geoLocation.get("GeographicCoordinates");
				String latitude = (String)geographicCoordinates.get("Latitude");
				String longitude = (String)geographicCoordinates.get("Longitude");
				
				Branch branch = new Branch(branchName, Float.parseFloat(latitude), Float.parseFloat(longitude), streetAddress,
						city, countrySubDivision, country, postcode);
				branches.add(branch);
			}
		}
		return branches;
	}
	
	/**
	 * This method will call the halifax url to get the JSON objects for all the branch and it 
	 * will transform the data into the list of Branch.  It will then filter to only return
	 * the branch that matches the giving city.
	 * 
	 * @return ArrayList<Branch>
	 */
	public List<Branch> getBranches(String city) {
		List<Branch> branchesCity = new ArrayList<Branch>();

		if (city != null) {
			List<Branch> branches = getAllBranches();
			branches.parallelStream().
	        filter(branch -> city.equalsIgnoreCase(branch.getCity())).
	        forEach((branch) -> {
	        	branchesCity.add(branch);
	        });
		}		
		return branchesCity;
	}
}
